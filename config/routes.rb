Rails.application.routes.draw do
  resources :messages
  get 'callback/index'
  post '/' => 'callback#recieved_data'
  get 'callback/recieved_data'
  root 'callback#index'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
